import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gpotcar_p1/models/product_model.dart';
import 'package:gpotcar_p1/views/product_details_view.dart';

class ProductsListView extends StatelessWidget {
  const ProductsListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Productos'),
        backgroundColor: Colors.pink,
      ),
      body: const ListaPerfumes(),
    );
  }
}

Future<String> getPerfumes(BuildContext context) async {
  var data = await DefaultAssetBundle.of(context).loadString("assets/json/perfumes.json");
  return data;
}

class ListaPerfumes extends StatelessWidget {
  const ListaPerfumes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: getPerfumes(context),
      builder: (context, AsyncSnapshot<String> snapshot) {
        if (snapshot.hasData) {
          var perfumes = Perfumes.fromJson(jsonDecode(snapshot.data!));
          var perfume = perfumes.perfumes.map((perf) {
            Color colores;
            switch (perf.sexo) {
              case "hombre":
                colores = const Color.fromARGB(255, 135, 157, 206).withOpacity(0.4);
                break;
              case "mujer":
                colores = const Color.fromARGB(255, 182, 109, 160).withOpacity(0.4);
                break;
              case "unisex":
                colores = const Color.fromARGB(255, 155, 209, 182).withOpacity(0.4);
                break;
              default:
                colores = const Color.fromARGB(255, 155, 209, 182).withOpacity(0.4);
            }

            return GestureDetector(
              onTap: () {
                _goToProductsDetailsView(context, perf);
              },
              child: Container(
                color: colores,
                child: ListTile(
                  leading: Container(
                    decoration: BoxDecoration(border: Border.all()),
                    child: Hero(
                      tag: '${perf.nombre}_image',
                      child: Image.asset(perf.url, width: 70, height: 70, fit: BoxFit.fill),
                    ),
                  ),
                  title: Text(perf.nombre),
                  subtitle: Text("${perf.precio}"),
                  trailing: const Icon(Icons.arrow_forward_ios),
                ),
              ),
            );
          }).toList();

          return ListView.builder(
            itemBuilder: (context, index) => perfume[index],
            itemCount: perfume.length,
          );
        } else {
          return const CircularProgressIndicator();
        }
      },
    );
  }

  void _goToProductsDetailsView(BuildContext context, Perfume perfume) {
    var route = MaterialPageRoute(
      builder: (context) => ProductDetailsView(perf: perfume),
    );
    Navigator.of(context).push(route);
  }
}
