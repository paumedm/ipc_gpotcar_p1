import 'package:flutter/material.dart';
import 'package:gpotcar_p1/models/product_model.dart';

class ProductDetailsView extends StatelessWidget {
  final Perfume perf;

  const ProductDetailsView({Key? key, required this.perf}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: const Text('Detalles del producto'),
          backgroundColor: Colors.pink,
        ),
        body: Column(
          children: [
            const SizedBox(height: 50),
            imagen(perf: perf),
            const SizedBox(height: 50),
            Expanded(child: infoperf(perf: perf)),
          ],
        ),
      );
}

Widget imagen({required Perfume perf}) => Container( 
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 50.0,
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Hero(
          tag: "${perf.nombre}_image",
          child: Image.asset(
            perf.url,
            width: 200,
            height: 200,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );

Widget infoperf({required Perfume perf}) => Container(
      width: double.infinity,    
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 100.0,
          ),
        ],
        color: Colors.white,
        borderRadius:  BorderRadius.only(
          topLeft: Radius.circular(20.0),
          topRight: Radius.circular(20.0),
        ),
        
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(20, 20, 20, 5),
            child: Text(
              perf.nombre,
              style: const TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 5, 20, 20),
            child: Text(
              perf.precio.toString() + "€",
              style: const TextStyle(
                fontSize: 40,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.all(20),
            child: Text(
              perf.descripcion,
              style: const TextStyle(color: Colors.black),
            ),
          ),
        ],
      ),
    );
