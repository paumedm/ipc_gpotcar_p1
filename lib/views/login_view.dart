import 'package:flutter/material.dart';
import 'package:gpotcar_p1/views/products_list_view.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Column(
            children: [
              header(),
              const SizedBox(height: 80),
              login(),
              const SizedBox(height: 200),
              button(context),
            ],
          ),
        ),
      );

  Widget header() {
    return Row(
      children: [
        Container(
           margin:const EdgeInsets.only(left: 20),
          child: const Text(
            'Login',
            textAlign: TextAlign.right,
            style: TextStyle(
              fontSize: 50,
              color: Color.fromARGB(255, 4, 4, 4),
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        const SizedBox(width: 70),
        buildCircleAvatar(60,40),
        buildCircleAvatar(80,80),
        buildCircleAvatar(20,20, marginRight: 10),
      ],
    );
  }

  Widget buildCircleAvatar(double width,double height, {double? marginRight}) {
    return Container(
      width: width,
      height: height,
      margin:const  EdgeInsets.only(top: 10),
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.0),
            blurRadius: 20.0,
          ),
        ],
        shape: BoxShape.circle,
        color: Colors.pink,
      ),
    );
  }

  Widget login()  {
    return Container(
      width: 300,
      height: 200,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: Colors.grey,
          width: 1,
        ),
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.0),
            blurRadius: 20.0,
          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          buildTextField('Username', Icons.person),
          buildTextField('Password', Icons.key, obscureText: true),
        ],
      ),
    );
  }

  Widget buildTextField(String labelText, IconData iconData,
      {bool obscureText = false}) {
    return SizedBox(
      width: 250,
      child: TextField(
        obscureText: obscureText,
        decoration: InputDecoration(
          labelText: labelText,
          icon: Icon(iconData),
        ),
      ),
    );
  }

  Widget button(BuildContext context) {
    return Container(
      alignment: Alignment.bottomRight,
      child: GestureDetector(
        onTap: () {
          _goToProductsListView(context);
        },
        child: Container(
          width: 200,
          height: 45,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
            color: Colors.pink,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              bottomLeft: Radius.circular(30),
            ),
            boxShadow:  [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 0.0),
                blurRadius: 20.0,
              ),
            ],
          ),
          child: const Text(
            "LOGIN",
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
            ),
          ),
        ),
      ),
    );
  }

  void _goToProductsListView(BuildContext context) {
    var route = MaterialPageRoute(
      builder: (context) => const ProductsListView(),
    );
    Navigator.of(context).push(route);
  }
}
