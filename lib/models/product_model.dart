
class Perfumes {
  late List<Perfume> perfumes;

  

  Perfumes.fromJson(Map<String, dynamic> json) {
    if (json['perfumes'] != null) {
      perfumes = <Perfume>[];
      json['perfumes'].forEach((v) {
        perfumes.add(Perfume.fromJson(v));
      });
    }
  }
}

class Perfume {
  late String nombre;
  late double precio;
  late String sexo;
  late String estilo;
  late String url;
  late String descripcion;

  Perfume(
      {required this.nombre,
      required this.precio,
      required this.sexo,
      required this.estilo,
      required this.url,
      required this.descripcion});

  Perfume.fromJson(Map<String, dynamic> json) {
    nombre = json['nombre'];
    sexo = json['sexo'];
    precio = json['precio'];
    estilo = json['estilo'];
    url = json['url'];
    descripcion = json['descripcion'];
  }
    
}