import 'package:flutter/material.dart';
import 'package:gpotcar_p1/views/login_view.dart';
import 'package:gpotcar_p1/models/product_model.dart';
import 'package:gpotcar_p1/views/products_list_view.dart';
import 'package:gpotcar_p1/views/product_details_view.dart';


void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const MaterialApp(
        title: 'P1 IPC',   
        debugShowCheckedModeBanner: false,  
        home: LoginView(),
      );
}